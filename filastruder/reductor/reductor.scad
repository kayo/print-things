use <involute_gear.scad>

$fn=200;

module reductor(z = [40, 10, 48, 10, 60, 10],
                m = [2.5, 2.0, 1.5],
                h = [30, 10, 10],
                x = 2,
                t = 0,
     ) {
     r1 = t * z[0];
     r2 = r1 / z[1] * z[2];
     r3 = r2 / z[3] * z[4];
     
     gear_single(z[0], m[0], h[0], -x, r1, 0) {
          gear_double(z[1], m[0], h[0], x, z[2], m[1], h[1], -x, 2, 1-r1, 90) {
               gear_double(z[3], m[1], h[1], x, z[4], m[2], h[2], -x, 2, 0.2+r2, 90) {
                    gear_single(z[5], m[2], h[2], x, -0.2-r3, 0);
               }
          }
     }
}

reductor(t = $t);
