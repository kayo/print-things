// Simple involute gear library
// Original author: RehelixK 2017
// ISO adaptation: Kayo 2017

function rad(a) = a * PI * 2 / 360;
function deg(a) = a * 360 / PI / 2;
function polar(r, a) = [r * cos(a), r * sin(a)];

function gear_diameter(z, m = 2.5) = m * z;
function gear_inner_diameter(z, m = 2.5, f = 0.7) = m * z - m * f * 2;
function gear_outer_diameter(z, m = 2.5, f = 0.7) = m * z + m * f * 2;
function gear_radius(z, m = 2.5) = m * z / 2;

module gear_involute_module(z = 30,   // Number of teeth
                            m = 2.5,  // Module of gear: 0.5, 0.7, 1, 1.25, 1.5, 1.75, 2, 2.5, 3, 3.5, 4, 4.5, 5, ... 50
                            f = 0.7,  // Dedendum factor: 1.25 or 0.7
                            s = 5,    // steps on each side of each tooth
                            t = 0.1,  // tollerance
     ) { // Note max X axis is centre of one of the teeth
    s = max(s, 3);
    d = m * z;
    b = m * f;
    r = d / 2 - b; // min radius
    pa = 360 / z; // pitch angle
    ta = 360 * t / (r * PI * 2); // tolerance angle
    pm = acos((r + b - t) / (r + b * 2)); // middle of involute
    da = deg(tan(pm)) - pm + pa / 4 - ta; // angle to make spacing right
    pt = acos((r - t) / (r + b * 2)); // top of involute
    ps = pt / s; // step for involute each side    
    points = [
         for(q = [0 : 1 : z - 1])
              for(e = [0 : 1 : 1])
                   for(a = [e ? -s : 0 : 1 : e ? 0 : s])
                        polar((r - t) / cos(a * ps),
                              q * pa + deg(tan(a * ps))
                              - a * ps + (e ? da : -da))
         ];
    polygon(points);    
}

// Modules to use the gear_involute 2D shape to make useful 3D objects
module gear_involute(z = 30,   // Number of teeth
                     m = 2.5,  // Module of gear: 0.5, 0.7, 1, 1.25, 1.5, 1.75, 2, 2.5, 3, 3.5, 4, 4.5, 5, ... 50
                     f = 0.7,  // Dedendum factor: 1.25 or 0.7
                     s = 5,    // Steps to make side of teeth
                     t = 0.1,  // Tolerance
                     h = 10,   // Height
                     x = 0,    // helix (1) or herringbone (2), -ve for other direction
     ) {
     d = m * z;
     if(x) {
          union() {
               q = abs(x);
               a = sign(x) * 360 * h / (d * PI) / q;
               for(l = [0 : 1 : q - 1])
                    translate([0, 0, l * h / q])
                         rotate([0, 0, ((l % 2) ? 1 : -1) * a / 2])
                         linear_extrude(height = h / q, twist = ((l % 2) ? 1 : -1) * a)
                         gear_involute_module(z, m, f, s, t);
          }
     } else {
          linear_extrude(height = h)
               gear_involute_module(z, m, f, s, t);
     }
}

module gear_translate(z1 = 0,   // Number of teeth for first gear
                      z2 = 0,   // Number of teeth for second gear
                      z = 0,    // Number of teeth
                      m = 2.5,  // Module of gear: 0.5, 0.7, 1, 1.25, 1.5, 1.75, 2, 2.5, 3, 3.5, 4, 4.5, 5, ... 50
                      h = 0,    // Height placement
                      d = 0,    // Direction
     ) {
     p = z ? gear_radius(z, m) : (gear_radius(z1, m) + gear_radius(z2, m));
     rotate([0, 0, d]) translate([p, 0, h]) children();
}

module gear_rotate(z = 30,   // Number of teeth
                   r = 0,    // Rotation in teeth
     ) {
     rotate([0, 0, r * 180 / z]) children();
}

module gear_single(z = 30,
                   m = 2.5,
                   h = 10,
                   x = 0,
                   r = 0,
                   d = 0,
     ) {
     gear_translate(z = z, m = m, d = 0) {
          gear_rotate(z = z, r = r) {
               gear_involute(z = z, m = m, x = x, h = h);
          }
     
          gear_translate(z = z, m = m, d = d) {
               children();
          }
     }
}

module gear_double(z1 = 30,
                   m1 = 2.5,
                   h1 = 10,
                   x1 = 0,
                   z2 = 30,
                   m2 = 2.5,
                   h2 = 10,
                   x2 = 0,
                   h = 2,
                   r = 0,
                   d = 0,
     ) {
     gear_translate(z = z1, m = m1) {
          gear_rotate(z = z1, r = r) {
               gear_involute(z = z1, m = m1, x = x1, h = h1);
               translate([0, 0, -h]) {
                    cylinder(d = gear_inner_diameter(z = min(z1, z2)), h = h);
                    translate([0, 0, -h2]) {
                         gear_involute(z = z2, m = m2, x = x2, h = h2);
                    }
               }
          }
          
          gear_translate(z = z2, m = m2, h = -h - h2, d = d) {
               children();
          }
     }
}
